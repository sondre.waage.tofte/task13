﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Exception2 : Exception
    {
        public Exception2()
        {
        }

        public Exception2(string message)
            : base(message)
        {
        }

        public Exception2(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
