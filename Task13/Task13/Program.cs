﻿using System;

namespace Task13
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<string> myChaosArray = new ChaosArray<string>();
            /*CreateArray that can store any type
             * Insert item that is randomly inserted
             * Retrive an item, that returns a random item from the array
             * Create exception2 for retrive from empty space
             */
             //Try to add something in the array
            try
            {
                myChaosArray.Insert("Hello");
            }
            catch (Exception1 ex)
            {
                Console.WriteLine(ex.Message);
            }
            //Try to retrive something from the array
            try
            {
                Console.WriteLine("You picked {0},", myChaosArray.Retrive());
            }
            catch (Exception2 ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
