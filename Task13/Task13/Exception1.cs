﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Exception1 : Exception
    {
        public Exception1()
        {
        }

        public Exception1(string message)
            : base(message)
        {
        }

        public Exception1(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
