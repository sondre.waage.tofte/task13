﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class ChaosArray<T>
    {
        //Create a constructor
        #region Start
        private string[] Array = new string[4];
        private T[] TArray = new T[4];

        T item1;
        T item2;
        T item3;
        T item4;

        public T Item1 { get => item1; set => item1 = value; }
        public T Item2 { get => item2; set => item2 = value; }
        public T Item3 { get => item3; set => item3 = value; }
        public T Item4 { get => item4; set => item4 = value; }
        #endregion
        //Insert message at a random spot in the TArray
        public void Insert(T message)
        {
            Random rnd = new Random();
            int random1 = rnd.Next(4);
            if (TArray[random1] == default)
            {
                Console.WriteLine("The array is empty at {0}", random1);
                TArray[random1] = message;
            }
            else
            {
                throw new Exception1("The space is not empty");
            }
        }
        //Retrive an item, that returns a random item from the array
        public T Retrive()
        {
            Random rnd = new Random();
            int random2 = rnd.Next(4);
            if (TArray[random2] == default)
            {
                Console.WriteLine("The array is empty at {0}", random2);
                throw new Exception2("The space is empty");
            }
            return TArray[random2];
        }
    }
}
