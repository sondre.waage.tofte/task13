﻿namespace Task13
{
    internal class Quad<T>
    {
        T item1;
        T item2;
        T item3;
        T item4;

        public T Item1 { get => item1; set => item1 = value; }
        public T Item2 { get => item2; set => item2 = value; }
        public T Item3 { get => item3; set => item3 = value; }
        public T Item4 { get => item4; set => item4 = value; }

        public T[] getAll()
        {

            T[] quadArray = new T[] { Item1, Item2, Item3, Item4 };

            return quadArray;

        }
    }
}